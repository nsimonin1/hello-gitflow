package org.simon.pascal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloGitflowsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloGitflowsApplication.class, args);
	}

}
