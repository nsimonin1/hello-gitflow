/**
 *
 */
package org.simon.pascal.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * @author ngos
 *
 */
@RestController
@RequestMapping("/")
@Slf4j
public class HelloController {

	@GetMapping
	public String hello() {
		log.info("home page");
		return "Gitflow project";
	}
}
